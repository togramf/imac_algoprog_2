#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size()<2)
        return;
    else {
        // initialisation
        Array& first = w->newArray(origin.size()/2);
        Array& second = w->newArray(origin.size()-first.size());

        // split
        for (int i=0; i<origin.size(); i++){
            if (i<origin.size()/2)
                first[i]=origin[i];
            else
                second[i-first.size()] = origin[i];
        }


        // recursiv splitAndMerge of lowerArray and greaterArray
        splitAndMerge(first);
        splitAndMerge(second);

        // merge
        merge(first, second, origin);
    }

}

void merge(Array& first, Array& second, Array& result)
{
    int a = 0, b=0;
    for (int i = 0; i<result.size(); i++){
        if ( a < first.size() && b < second.size()){
            if (first[a] < second[b]){
                result[i] = first[a];
                a++;
            } else {
                result[i] = second[b];
                b++;
            }
        } else if (a >= first.size()){
            result[i] = second[b];
            b++;
        } else {
            result[i] = first[a];
            a++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
