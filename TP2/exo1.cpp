#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;


uint searchIndexMin (const Array& toSort, const uint start)
{
    uint min = start;
    for (uint i=start+1; i<toSort.size(); i++){
        if (toSort[min] > toSort[i])
                min = i;
    }
    return min;
}

void selectionSort(Array& toSort){
    for (uint i=0; i<toSort.size(); i++)
    {
        uint minIndex = searchIndexMin(toSort, i);
        toSort.swap(i, minIndex);

    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
