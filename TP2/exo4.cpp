#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (size <= 1)
        return;
    else {
        Array& lowerArray = w->newArray(size-1);
        Array& greaterArray= w->newArray(size-1);
        int lowerSize = 0, greaterSize = 0; // effectives sizes
        int pivot = toSort[0];

        // split
        for (int i=1; i<size; i++){
            if(toSort[i] <= pivot){
                lowerArray.set(lowerSize, toSort[i]);
                lowerSize++;
            } else if (toSort[i] > pivot){
                greaterArray.set(greaterSize, toSort[i]);
                greaterSize++;
            }
        }
        // recursiv sort of lowerArray and greaterArray
        recursivQuickSort(lowerArray, lowerSize);
        recursivQuickSort(greaterArray, greaterSize);

        // merge
        int i=0;
        while (i<lowerSize){
            toSort.set(i,lowerArray[i]);
            i++;
        }
        toSort.set(i, pivot);
        i++;
        while(i-lowerSize-1 < greaterSize){
            toSort.set(i, greaterArray[i-lowerSize-1]);
            i++;
        }
    }

}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
