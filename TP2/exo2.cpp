#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

uint findInsertIndex(Array& sorted, int n, int size)
{
    int i;
    for (i = 0; i < size; i++){
        if (sorted[i]> n)
                return i;
    }
    return i;
}

void insertionSort(Array& toSort)
{
	Array& sorted=w->newArray(toSort.size());

	// insertion sort from toSort to sorted
    sorted[0] = toSort[0];
    for (uint i = 1; i<toSort.size(); i++){
        sorted.insert(findInsertIndex(sorted, toSort[i], i), toSort[i]);
    }
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
