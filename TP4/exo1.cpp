#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
}

/*insert nouveau node dans le tas heap en gardant la propriete de tas*/
void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    (*this)[i] = value;
    while (i>0 && this->get(i)>this->get((i-1)/2)){
        this->swap(i,(i-1)/2);
        i = (i-1)/2;
    }
}

/* Si noeud d'indice nodeIndex n'est pas plus grand que ses enfants
reconstruire le tas a partir de cet indice */
void Heap::heapify(int heapSize, int nodeIndex)
{
    int right = this->rightChild(nodeIndex);
    int left = this->leftChild(nodeIndex);
    int largest = nodeIndex;

    if (left < heapSize && this->get(left) > this->get(largest))
        largest = left;
    if (right < heapSize && this->get(right)> this->get(largest))
        largest = right;

    if (largest != nodeIndex){
        this->swap(nodeIndex, largest);
        this->heapify(heapSize, largest);
    }

}

/*Construit un tas a partir des valeurs de numbers
(avec insertHeapNode ou heapify) */
void Heap::buildHeap(Array& numbers)
{
    for (int i=0; i<numbers.size(); i++){
        this->insertHeapNode(this->size(),numbers[i]) ;
    }
}

/*Construit un tableau trie a partir d'un tas*/
void Heap::heapSort()
{
    for (int i = this->size()-1; i>=0; i--){
        this->swap(0,i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
