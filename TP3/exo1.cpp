#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/*retourne l'index de la valeur toSearch
* -1 si la valeur n'est pas dans le tableau */
int binarySearch(Array& array, int toSearch)
{
    int start = 0;
    int end  = array.size();
    int mid;
    while (start < end){
        mid = (start+end)/2;
        if (array[mid]>toSearch)
            end = mid;
        else if (array[mid]<toSearch)
            start = mid + 1;
        else
            return mid;
    }
    return -1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
