#include <iostream>
using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

/*possede pointeur sur premier noeud, null au debut
a l'ajout vérifier si premier noeuds existe, si oui ajouter apres
sinon creer */
struct Liste{
    Noeud* premier;
    int taille; 
    int capacite; 
};

void initialise (Liste* liste){
    liste->capacite = 10;
    liste->premier = nullptr;
    liste->taille = 0;
}

bool est_vide(const Liste* liste){
    if (liste->taille == 0)
        return true;
    return false;
}

void ajoute (Liste* liste, int valeur){
    Noeud* nouveau = new Noeud();
    nouveau->donnee = valeur;
    nouveau->suivant = nullptr;

    if (est_vide(liste)){
        liste->premier = nouveau;
    } else {
        if (liste->taille >= liste->capacite){
            liste->capacite += 10;
        }
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
            dernier=dernier->suivant;
        dernier->suivant = nouveau;
    }

    liste->taille ++;
}

void affiche (const Liste* liste){
    Noeud* noeud = liste->premier;
    while (noeud != nullptr){
        cout << noeud->donnee <<" ";
        noeud = noeud->suivant;
    }
}

int recupere (const Liste* liste, int n){
    int indice = 0;
    Noeud* noeud = liste->premier;
    while (indice <n){
        if (noeud->suivant != nullptr){
            noeud = noeud->suivant;
            indice ++;
        } else {
            return -1;
        }
    }
    return noeud->donnee;
}

int cherche (const Liste* liste, int valeur){
    int indice = 1;
    Noeud* noeud = liste->premier;
    while (noeud->suivant != nullptr){
        if (noeud->donnee == valeur)
            return indice;
        else {
            noeud = noeud->suivant;
            indice ++;
        }
    }
    return -1;
}

void stocke (Liste* liste, int n, int valeur){
    int indice = 0;
    Noeud* noeud = liste->premier;
    while (indice < n && noeud->suivant != nullptr){
        noeud = noeud->suivant;
        indice ++;
    }
    if (indice == n)
        noeud->donnee = valeur;
}

void pousse_file(Liste* liste, int valeur){
    //ajoute valeur a la fin de la structure
    ajoute(liste, valeur);
}

void pousse_pile(Liste* liste, int valeur){
    //ajoute valeur au debut de la structure
    Noeud* noeud = new Noeud();
    noeud->donnee = valeur;
    noeud->suivant = liste->premier;
    liste->premier = noeud;
    liste->taille ++;
}

int retire_file(Liste* liste){
    //enleve et retourne premiere valeur ajoutee 
    if (!est_vide(liste)){
        Noeud* noeud = liste->premier;
        liste->premier = liste->premier->suivant;
        liste->taille --;
        return noeud->donnee;
    }
    return -1;
}

int retire_pile(Liste* liste){
    //enleve et retourne derniere valeur ajoutee 
    return retire_file(liste);
}

int main() {
    Liste liste;
    initialise (&liste);

    if (!est_vide(&liste))
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    else 
        cout << "liste ok"<< endl;

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
    }

    if (est_vide(&liste))
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    else 
        cout << "liste ok, taille = "<< liste.taille <<endl;
    
    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    std::cout << std::endl;
    
    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    
    stocke(&liste, 4, 7);
    std::cout << "Elements après stockage de 7 en position 5:" << std::endl;
    affiche(&liste);
    std::cout << std::endl;

    Liste pile;
    Liste file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    std::cout << "Elements FILE:" << std::endl;
    affiche(&file);
    std::cout << "taille : "<< file.taille<< std::endl;

    std::cout << "Vide FILE:" << std::endl;
    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << "taille FILE : " << file.taille<<std::endl;
    }

    std::cout << "Elements PILE:" << std::endl;
    affiche(&pile);
    std::cout << "taille : "<< pile.taille<< std::endl;

    std::cout << "Vide PILE:" << std::endl;
    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}