#include <iostream>
using namespace std;

/*STRUCTURES*/
struct Noeud{
    int donnee;
    Noeud* suivant;
};

/*possede pointeur sur premier noeud, null au debut
a l'ajout vérifier si premier noeux existe, si oui ajouter apres
sinon creer */
struct Liste{
    Noeud* premier;
    int taille;
};

/*tableau qui se réalloue à chq fois quon depasse la capacite
possede taille pour contabiliser nb d'elemt enregistres
possede capacité qui reference nb max d'elemet stockable, se met a jour qd realloc*/
struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};

/*FONCTIONS LISTES*/

void initialise (Liste* liste)
{
    liste->premier = nullptr;
    liste->taille = 0;
}

bool est_vide(const Liste* liste)
{
    if (liste->taille == 0)
        return true;
    return false;
}

//ajoute une valeur a la fin de la liste (gere memoire)
void ajoute (Liste* liste, int valeur)
{
    Noeud* nouveau = new Noeud();
    nouveau->donnee = valeur;
    nouveau->suivant = nullptr;

    if (est_vide(liste)){
        liste->premier = nouveau;
    } else {
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
            dernier=dernier->suivant;
        dernier->suivant = nouveau;
    }

    liste->taille ++;
}

void affiche (const Liste* liste)
{
    Noeud* noeud = liste->premier;
    while (noeud != nullptr){
        cout << noeud->donnee <<" ";
        noeud = noeud->suivant;
    }
    cout << endl;
}

//retourne n ieme entier de la structure
int recupere (const Liste* liste, int n)
{
    int indice = 0;
    Noeud* noeud = liste->premier;
    while (indice <n){
        if (noeud->suivant != nullptr){
            noeud = noeud->suivant;
            indice ++;
        } else {
            return -1;
        }
    }
    return noeud->donnee;
}

//retourne l'index de la valeur dans la structure, si ne le trouve pas -1
int cherche (const Liste* liste, int valeur)
{
    int indice = 1;
    Noeud* noeud = liste->premier;
    while (noeud->suivant != nullptr){
        if (noeud->donnee == valeur)
            return indice;
        else {
            noeud = noeud->suivant;
            indice ++;
        }
    }
    return -1;
}

//redefini n ieme valeur de la structure
void stocke (Liste* liste, int n, int valeur)
{
    int indice = 0;
    Noeud* noeud = liste->premier;
    while (indice < n && noeud->suivant != nullptr){
        noeud = noeud->suivant;
        indice ++;
    }
    if (indice == n)
        noeud->donnee = valeur;
}

/*FONCTIONS PILES ET FILES AVEC LISTES*/
//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    //ajoute valeur a la fin de la structure
    ajoute(liste, valeur);
}


//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    //enleve et retourne premiere valeur ajoutee
    if (!est_vide(liste)){
        Noeud* noeud = liste->premier;
        liste->premier = liste->premier->suivant;
        liste->taille --;
        return noeud->donnee;
    }
    return -1;
}

// ajoute valeur au debut de la structure
void pousse_pile(Liste* liste, int valeur)
{
    //ajoute valeur au debut de la structure
    Noeud* noeud = new Noeud();
    noeud->donnee = valeur;
    noeud->suivant = liste->premier;
    liste->premier = noeud;
    liste->taille ++;
}

//enleve derniere valeur ajoutee et la retourne
int retire_pile(Liste* liste)
{
    //enleve et retourne derniere valeur ajoutee
    return retire_file(liste);
}

/*FONCTIONS TABLEAUX*/

void initialise(DynaTableau* tableau, int capacite){
    tableau->capacite = capacite;
    tableau->taille = 0;
    tableau->donnees = new int[capacite];
}

bool est_vide(const DynaTableau* tableau){
    if (tableau->taille == 0)
        return true;
    return false;
}

void ajoute(DynaTableau* tableau, int valeur){
    if (tableau->taille >= tableau->capacite){
        tableau->capacite += 5;
        tableau->donnees = (int*) realloc(tableau->donnees, sizeof(int*)*tableau->capacite);
    }
    tableau->donnees[tableau->taille] = valeur;
    tableau->taille ++;
}

void affiche(const DynaTableau * tableau){
    for (int i=0; i<tableau->taille; i++){
        cout << tableau->donnees[i]<<" ";
    }
    cout<<endl;
}

int recupere(const DynaTableau* tableau, int n){
    if (n<tableau->taille){
        return tableau->donnees[n];
    }
    return -1;
}

int cherche (const DynaTableau* tableau, int valeur){
    for (int i=0; i<tableau->taille; i++){
        if (tableau->donnees[i] == valeur)
            return i+1;
    }
    return -1;
}

void stocke (DynaTableau* tableau, int n, int valeur){
    if (n < tableau->taille)
        tableau->donnees[n] = valeur;
    else if (n == tableau->taille)
        ajoute(tableau, valeur);
}


/*FONCTIONS PILES ET FILES AVEC TABLEAUX*/
void pousse_pile(DynaTableau* tableau, int valeur){
    //ajoute à la fin
    ajoute(tableau, valeur);
}

void pousse_file(DynaTableau* tableau, int valeur){
    //ajoute debut
    ajoute(tableau, tableau->donnees[tableau->taille-1]);
    for (int i = tableau->taille-1; i>0; i--){
        tableau->donnees[i]=tableau->donnees[i-1];
    }
    tableau->donnees[0] = valeur;
}

int retire_file (DynaTableau* tableau) {
    //retire premier ajoute cad fin de tableau
    if (!est_vide(tableau)){
        int valeur = tableau->donnees[tableau->taille-1];
        tableau->taille--;
        return valeur;
    }
    return -1;
}

int retire_pile (DynaTableau* tableau){
    //retire dernier ajoute cad fin de tableau
    return retire_file(tableau);
}

/*MAIN*/
int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    else
        cout << "liste ok"<<endl;

    if (!est_vide(&tableau))
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    else
        cout << "tableau ok" << endl;

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    else
        cout << "taille liste : "<<liste.taille<<endl;

    if (est_vide(&tableau))
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    else
        cout<< "taille tableau : "<<tableau.taille<<endl;

    std::cout << "Elements initiaux:" << std::endl;
    cout <<"Liste : ";
    affiche(&liste);
    cout <<"Tableau : ";
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7 en position 5 :" << std::endl;
    cout <<"Liste : ";
    affiche(&liste);
    cout <<"Tableau : ";
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    cout <<endl<< "PILE : ";
    affiche(&pile);

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    cout << endl<< "FILE : ";
    affiche(&file);

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }


    DynaTableau tab_pile;
    DynaTableau tab_file;

    initialise(&tab_pile,5);
    initialise(&tab_file,5);

    for (int i=1; i<=7; i++) {
        pousse_file(&tab_file, i);
        pousse_pile(&tab_pile, i);
    }

    cout <<endl<< "TABLEAU PILE : ";
    affiche(&tab_pile);

    compteur = 10;
    while(!est_vide(&tab_pile) && compteur > 0)
    {
        std::cout << retire_pile(&tab_pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    cout <<endl<< "TABLEAU FILE : ";
    affiche(&tab_file);

    compteur = 10;
    while(!est_vide(&tab_file) && compteur > 0)
    {
        std::cout << retire_file(&tab_file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
