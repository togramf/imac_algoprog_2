#include <iostream>
using namespace std;

/*tableau qui se réalloue à chq fois quon depasse la capacite
possede taille pour contabiliser nb d'elemt enregistres
possede capacité qui reference nb max d'elemet stockable, se met a jour qd realloc*/
struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};

void initialise(DynaTableau* tableau, int capacite){
    tableau->capacite = capacite;
    tableau->taille = 0;
    tableau->donnees = new int[capacite];
}

bool est_vide(const DynaTableau* tableau){
    if (tableau->taille == 0)
        return true;
    return false;
}

void ajoute(DynaTableau* tableau, int valeur){
    if (tableau->taille >= tableau->capacite){
        tableau->capacite += 5;
        tableau->donnees = (int*) realloc(tableau->donnees, sizeof(int*)*tableau->capacite);
    }
    tableau->donnees[tableau->taille] = valeur;
    tableau->taille ++;
}

void affiche(const DynaTableau * tableau){
    for (int i=0; i<tableau->taille; i++){
        cout << tableau->donnees[i]<<" ";
    }
    cout<<endl;
}

int recupere(const DynaTableau* tableau, int n){
    if (n<tableau->taille){
        return tableau->donnees[n];
    }
    return -1;
}

int cherche (const DynaTableau* tableau, int valeur){
    for (int i=0; i<tableau->taille; i++){
        if (tableau->donnees[i] == valeur)
            return i+1;
    }
    return -1;
}

void stocke (DynaTableau* tableau, int n, int valeur){
    if (n < tableau->taille)
        tableau->donnees[n] = valeur;
    else if (n == tableau->taille)
        ajoute(tableau, valeur);
}

/*FONCTIONS PILES ET FILES AVEC TABLEAUX*/
void pousse_pile(DynaTableau* tableau, int valeur){
    //ajoute à la fin 
    ajoute(tableau, valeur);
}

void pousse_file(DynaTableau* tableau, int valeur){
    //ajoute debut 
    ajoute(tableau, tableau->donnees[tableau->taille-1]);
    for (int i = tableau->taille-1; i>0; i--){
        tableau->donnees[i]=tableau->donnees[i-1];
    }
    tableau->donnees[0] = valeur;
}

int retire_file (DynaTableau* tableau) {
    //retire premier ajoute cad fin de tableau
    if (!est_vide(tableau)){
        int valeur = tableau->donnees[tableau->taille-1];
        tableau->taille--;
        return valeur;
    }
    return -1;   
}

int retire_pile (DynaTableau* tableau){
    //retire dernier ajoute cad fin de tableau
    return retire_file(tableau);
}  

int main(){
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&tableau))
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    else 
        cout << "tableau ok" << endl;

    for (int i=1; i<=7; i++) {
        ajoute(&tableau, i*5);
    }
    if (est_vide(&tableau))
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    
    std::cout << "Elements initiaux:" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&tableau, 4, 7);
    std::cout << "Elements après stockage de 7 en position 5 :" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    DynaTableau tab_pile;
    DynaTableau tab_file;

    initialise(&tab_pile,5);
    initialise(&tab_file,5);

    for (int i=1; i<=7; i++) {
        pousse_file(&tab_file, i);
        pousse_pile(&tab_pile, i);
    }

    cout <<endl<< "TABLEAU PILE : ";
    affiche(&tab_pile);

    int compteur = 10;
    while(!est_vide(&tab_pile) && compteur > 0)
    {
        std::cout << retire_pile(&tab_pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    cout <<endl<< "TABLEAU FILE : ";
    affiche(&tab_file);

    compteur = 10;
    while(!est_vide(&tab_file) && compteur > 0)
    {
        std::cout << retire_file(&tab_file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0; 
}

